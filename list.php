<?php
require_once __DIR__ . '/functions.php';

if (empty($_SESSION['user'])){
    header($_SERVER["SERVER_PROTOCOL"] . '403 Forbidden Error');
    die;
}

if (!empty($_SESSION['user'])) {?>
<div>Добро пожаловать, <?= getAuthorizedUser()['username']; ?></div>
<br>
<form action="index.php" method="GET">
    <div><input type="submit" name="exit" value="Выход"><div>
</form>
<br>
<?php }

if (!empty($_GET) && isset($_GET)):
    $number = $_GET["test_number"];
    if (!file_exists(__DIR__ . '/Tests/test' . $number . '.json')):
        header($_SERVER["SERVER_PROTOCOL"] . '404 Not Found');
        exit;
    endif;
    unlink(__DIR__ . '/Tests/test' . $number . '.json');
    redirect('list');
endif;?>
    <p>Список доступных тестов: </p>
<?php
$test_files = glob('Tests/*.json');
if (!empty($test_files)):
    foreach ($test_files as $file): ?>
        <p><?php echo basename($file)?></p>
    <?php endforeach;
endif;

if (!empty($_SESSION['user'])) {?>
    <form action="test.php" method="GET">
        <div>Введите номер теста <input type="text" name="test_number"></div>
        <br>
        <div><input type="submit" name="get test" value="Пройти тест"><div>
    </form>
    <br>
<?php
    if (!empty($_SESSION['user']['password'])) {?>
        <p>Выберите .json файл с тестом для загрузки на сервер:</p>
        <form action="admin.php" method="POST" enctype="multipart/form-data">
            <div><input type="file" name="test"></div>
            <br>
            <div><input type="submit" value="Отправить"></div>
            <br>
        </form>
        <form action="" method="GET">
            <br>
            <div>Введите номер теста <input type="text" name="test_number"></div>
            <br>
            <div><input type="submit" name="delete test" value="Удалить тест"><div>
        </form>
    <?php }
}
?>
