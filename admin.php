<?php
require_once __DIR__ . '/functions.php';

if (empty($_SESSION['user']['password'])){
    header($_SERVER["SERVER_PROTOCOL"] . '403 Forbidden Error');
    die;
}

$upload_dir = 'Tests/';
if (!empty($_FILES) && array_key_exists('test', $_FILES)) {
    if (move_uploaded_file($_FILES['test']['tmp_name'], $upload_dir . $_FILES['test']['name'])) {
        header('Location: list.php');
        exit;
    }
    else {
        echo ' Ошибка загрузки файла. ';
    }
}
?>