<?php
require_once __DIR__ . '/functions.php';

if (!empty($_POST) && isset($_POST) && !empty($_POST["name"])):
    $file_name = $_POST["test_number"];
    $file = json_decode(file_get_contents($file_name), true);
    $i = 1;
    $right_answer = 0;
    foreach ($file as $question):
        if ($_POST[$i] == $question["correct"]):
            $right_answer++;
        endif;
        $i++;
    endforeach;
    $score = $right_answer / ($i - 1) * 100;
    header('Content-type: image/png');
    $img = imagecreatefrompng('Diplom-H05.png');
    $name = $_POST['name'];
    $text_color = imagecolorallocate($img, 184, 134, 11);
    $font_file = __DIR__ . '/font/font.ttf';
    imagettftext($img, 70, 0, 900, 850, $text_color, $font_file, $name);
    imagettftext($img, 70, 0, 930, 1000, $text_color, $font_file, 'Вы набрали');
    imagettftext($img, 70, 0, 950, 1150, $text_color, $font_file, $score . ' баллов.');
    imagepng($img);
    imagedestroy($img);
    exit;
endif;

if (!empty($_GET) && isset($_GET)):
    $number = $_GET["test_number"];
    if (!file_exists(__DIR__ . '/Tests/test' . $number . '.json')):
        header($_SERVER["SERVER_PROTOCOL"] . '404 Not Found');
        exit;
    endif;
    $file = json_decode(file_get_contents('test' . $number . '.json'), true); ?>
    <form action="test.php" method="POST">
        <?php foreach ($file as $question): ?>
            <fieldset>
                <legend><?php echo $question["quest"]; ?></legend>
                <?php foreach ($question["answer"] as $answer ): ?>
                    <label><input name="<?php echo $question["number"]; ?>" type="radio" value="<?php echo $answer; ?>"><?php echo $answer; ?></label>
                <?php endforeach;?>
            </fieldset>
            <br>
        <?php endforeach;?>
        <input type="hidden" name="name" value="<?= getAuthorizedUser()['username']; ?>">
        <input type="hidden" name="test_number" value="<?php echo 'test' . $number . '.json';?>">
        <input type="submit" name="get result" value="Показать результат">
    </form>
<?php endif;
if (!empty($_SESSION['user']['password'])):?>
    <div><a href="list.php"><button>Вернуться к выбору и загрузке тестов</button></a></div>
<?php endif; ?>

